BLTouch Spacer
==============

I need a significant negative Z-offset with my printer.  These spacers
should take up most of that, allowing use of an offset closer to zero.

FWIW, I'm using an Anet A8, with the BLTouch mounted in one of these:

https://www.thingiverse.com/thing:2360242/

This should be adaptable to any BLTouch installation; just customize it for
the offset you need.
