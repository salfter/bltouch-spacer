thickness=1.0;
$fn=90;

translate([0,0,-18+thickness])
difference()
{
    intersection()
    {
        translate([241.6,-197,0])
            import("../BLTouch holder/Anet_Fan_Sensor_Holder_BLT_Leo_N.stl");

        cylinder(d=28, h=18);
    }
    cylinder(d=28, h=18-thickness);
    translate([-9,0,0])
        cylinder(d=3.4, h=18);
    translate([9,0,0])
        cylinder(d=3.4, h=18);
}
